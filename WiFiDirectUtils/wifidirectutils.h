#pragma once

#include <iterator>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <winrt/base.h>
#include <winrt/Windows.Foundation.h>
#include <winrt/Windows.Foundation.Collections.h>
#include <winrt/Windows.Devices.Enumeration.h>
#include <QDebug>
#include <QSettings>
#include "wifidirectutils_global.h"

using namespace winrt;
using namespace winrt::Windows::Foundation;
using namespace winrt::Windows::Foundation::Collections;
using namespace winrt::Windows::Devices::Enumeration;

QDebug operator<<(QDebug dbg, const std::string& str);
QDebug operator<<(QDebug dbg, const hstring& str);
QDebug operator<<(QDebug dbg, DevicePairingResultStatus status);
QDebug operator<<(QDebug dbg, DeviceUnpairingResultStatus status);
QDebug operator<<(QDebug dbg, AsyncStatus status);
QDebug operator<<(QDebug dbg, DeviceWatcherStatus status);

void installAppMessageHandler();

class Settings
{
public:
	Settings(const QString& appName);
	std::vector<hstring> devicesToFind();

private:
	std::shared_ptr<QSettings> _settings;
};

void dumpProperties(const IMapView<hstring, winrt::Windows::Foundation::IInspectable>& properties);

std::wstring join(std::vector<hstring> vector, const std::wstring& separator = L", ");

class WIFIDIRECTUTILS_EXPORT WiFiDirectUtils
{
public:
	WiFiDirectUtils();
};
