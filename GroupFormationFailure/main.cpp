#include <QtCore/QCoreApplication>
#include <winrt/Windows.Devices.WiFiDirect.h>
#include "watcher.h"
#include "wifidirectutils.h"

using namespace winrt;
using namespace Windows::Foundation;
using namespace Windows::Devices::WiFiDirect;

static std::mutex mutex;

void pairingRequested(const DeviceInformationCustomPairing& sender, const DevicePairingRequestedEventArgs& args)
{
	std::unique_lock<std::mutex> lock(mutex);
	qDebug() << "Pairing requested...";
	if (args.PairingKind() == DevicePairingKinds::ConfirmOnly)
	{
		args.Accept();
	}
	else
	{
		qDebug() << "Invalid pairing kind" << static_cast<int>(args.PairingKind());
		throw std::runtime_error("");
	}
}

void pair(const DeviceInformation& discoveredDevice)
{
	const DeviceInformationCustomPairing& customPairing = discoveredDevice.Pairing().Custom();

	auto connectionParams = WiFiDirectConnectionParameters();
	connectionParams.GroupOwnerIntent(4);

	auto status = customPairing.PairAsync(DevicePairingKinds::ConfirmOnly, DevicePairingProtectionLevel::Default, connectionParams).get().Status();
	if (status == DevicePairingResultStatus::Paired)
	{
		qDebug() << "Devices successfully paired";
	}
	else
	{
		qDebug() << "Pair failed, status" << status;
		throw std::runtime_error("");
	}

	if (!discoveredDevice.Pairing().IsPaired())
	{
		qDebug() << "Pair failed";
		throw std::runtime_error("");
	}
}

void unpair(const DeviceInformation& discoveredDevice)
{
	if (!discoveredDevice.Pairing().IsPaired())
	{
		return;
	}

	auto status = discoveredDevice.Pairing().UnpairAsync().get().Status();
	if (status == DeviceUnpairingResultStatus::Unpaired)
	{
		qDebug() << "Device successfully unpaired";
	}
	else
	{
		qDebug() << "Unpair failed, status" << status;
		throw std::runtime_error("");
	}

	if (discoveredDevice.Pairing().IsPaired())
	{
		qDebug() << "Unpair failed";
		throw std::runtime_error("");
	}
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	installAppMessageHandler();
	init_apartment();

	Settings settings(argv[0]);

	try
	{
		WiFiDirectDeviceWatcher watcher(settings.devicesToFind(), mutex);
		watcher.discover();
		if (watcher.discoveredDevice() == nullptr)
		{
			qDebug() << "Failed to find devices.";
			throw std::runtime_error("");
		}

		const DeviceInformation& discoveredDevice = watcher.discoveredDevice();
		const DeviceInformationCustomPairing& customPairing = discoveredDevice.Pairing().Custom();
		customPairing.PairingRequested(pairingRequested);

		unpair(discoveredDevice);
		pair(discoveredDevice);
		unpair(discoveredDevice);
	}
	catch (const std::exception& e)
	{
		qDebug() << e.what();
		throw;
	}
	catch (...)
	{
		qDebug() << "Unknown exception";
		throw;
	}

	return a.exec();
}
