#include <fstream>
#include <iostream>
#include <QMutex>
#include <QFileInfo>
#include "wifidirectutils.h"

QtMessageHandler defaultMessageHandler;

QDebug operator<<(QDebug dbg, const std::string& str)
{
	return dbg << QString::fromLocal8Bit(str.data());
}

QDebug operator<<(QDebug dbg, const hstring& str)
{
	return dbg << QString::fromWCharArray(str.c_str());
}

QDebug operator<<(QDebug dbg, DevicePairingResultStatus status)
{
	switch (status)
	{
	case DevicePairingResultStatus::Paired: return dbg << "Paired";
	case DevicePairingResultStatus::NotReadyToPair: return dbg << "NotReadyToPair";
	case DevicePairingResultStatus::NotPaired: return dbg << "NotPaired";
	case DevicePairingResultStatus::AlreadyPaired: return dbg << "AlreadyPaired";
	case DevicePairingResultStatus::ConnectionRejected: return dbg << "ConnectionRejected";
	case DevicePairingResultStatus::TooManyConnections: return dbg << "TooManyConnections";
	case DevicePairingResultStatus::HardwareFailure: return dbg << "HardwareFailure";
	case DevicePairingResultStatus::AuthenticationTimeout: return dbg << "AuthenticationTimeout";
	case DevicePairingResultStatus::AuthenticationNotAllowed: return dbg << "AuthenticationNotAllowed";
	case DevicePairingResultStatus::AuthenticationFailure: return dbg << "AuthenticationFailure";
	case DevicePairingResultStatus::NoSupportedProfiles: return dbg << "NoSupportedProfiles";
	case DevicePairingResultStatus::ProtectionLevelCouldNotBeMet: return dbg << "ProtectionLevelCouldNotBeMet";
	case DevicePairingResultStatus::AccessDenied: return dbg << "AccessDenied";
	case DevicePairingResultStatus::InvalidCeremonyData: return dbg << "InvalidCeremonyData";
	case DevicePairingResultStatus::PairingCanceled: return dbg << "PairingCanceled";
	case DevicePairingResultStatus::OperationAlreadyInProgress: return dbg << "OperationAlreadyInProgress";
	case DevicePairingResultStatus::RequiredHandlerNotRegistered: return dbg << "RequiredHandlerNotRegistered";
	case DevicePairingResultStatus::RejectedByHandler: return dbg << "RejectedByHandler";
	case DevicePairingResultStatus::RemoteDeviceHasAssociation: return dbg << "RemoteDeviceHasAssociation";
	case DevicePairingResultStatus::Failed: return dbg << "Failed";
	}
	return dbg << static_cast<int>(status);
}

QDebug operator<<(QDebug dbg, DeviceUnpairingResultStatus status)
{
	switch (status)
	{
	case DeviceUnpairingResultStatus::Unpaired: return dbg << "Unpaired";
	case DeviceUnpairingResultStatus::AlreadyUnpaired: return dbg << "AlreadyUnpaired";
	case DeviceUnpairingResultStatus::OperationAlreadyInProgress: return dbg << "OperationAlreadyInProgress";
	case DeviceUnpairingResultStatus::AccessDenied: return dbg << "AccessDenied";
	case DeviceUnpairingResultStatus::Failed: return dbg << "Failed";
	};
	return dbg << static_cast<int>(status);
}

QDebug operator<<(QDebug dbg, AsyncStatus status)
{
	switch (status)
	{
	case AsyncStatus::Started: return dbg << "Started";
	case AsyncStatus::Completed: return dbg << "Completed";
	case AsyncStatus::Canceled: return dbg << "Canceled";
	case AsyncStatus::Error: return dbg << "Error";
	};
	return dbg << static_cast<int>(status);
}

QDebug operator<<(QDebug dbg, DeviceWatcherStatus status)
{
	switch (status)
	{
	case DeviceWatcherStatus::Created: return dbg << "Created";
	case DeviceWatcherStatus::Started: return dbg << "Started";
	case DeviceWatcherStatus::EnumerationCompleted: return dbg << "EnumerationCompleted";
	case DeviceWatcherStatus::Stopping: return dbg << "Stopping";
	case DeviceWatcherStatus::Stopped: return dbg << "Stopped";
	case DeviceWatcherStatus::Aborted: return dbg << "Aborted";
	};
	return dbg << static_cast<int>(status);
}

void appMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	static QMutex mutex;
	QMutexLocker lock(&mutex);

	static std::ofstream logFile("logFile.txt");
	if (logFile)
	{
		logFile << qPrintable(qFormatLogMessage(type, context, msg)) << std::endl;
	}

	if (defaultMessageHandler != NULL)
	{
		defaultMessageHandler(type, context, msg);
	}
}

void installAppMessageHandler()
{
	qSetMessagePattern("%{time} (%{file}:%{line}) - %{message}");
	defaultMessageHandler = qInstallMessageHandler(appMessageHandler);
}

Settings::Settings(const QString& appName)
{
	const QString fileName = QFileInfo(appName).baseName() + ".ini";
	_settings = std::make_shared<QSettings>(fileName, QSettings::IniFormat);
	if (_settings->status() == QSettings::FormatError)
	{
		throw std::runtime_error((fileName + " format error").toLocal8Bit().constData());
	}
}

std::vector<hstring> Settings::devicesToFind()
{
	const QString prefix("device_names");
	std::vector<hstring> deviceNames;

	int size = _settings->beginReadArray(prefix);
	if (size)
	{
		for (int index = 0; index < size; index++) {
			_settings->setArrayIndex(index);
			deviceNames.push_back(_settings->value("device_name").toString().toStdWString());
		}
		_settings->endArray();
	}
	else
	{
		_settings->endArray();
		_settings->beginWriteArray(prefix);
		int index = 0;

		std::cout << "Please enter device names one by one, enter empty string to complete: ";
		std::wstring name;
		std::getline(std::wcin, name);
		while (!name.empty())
		{
			deviceNames.push_back(name);
			_settings->setArrayIndex(index++);
			_settings->setValue("device_name", QString::fromStdWString(name));
			std::getline(std::wcin, name);
		}
		_settings->endArray();
		_settings->sync();
	}

	return deviceNames;
}

void dumpProperties(const IMapView<hstring, winrt::Windows::Foundation::IInspectable>& properties)
{
	for (IKeyValuePair<hstring, winrt::Windows::Foundation::IInspectable> prop : properties)
	{
		if (prop.Value() == nullptr)
		{
			qDebug() << " " << prop.Key() << "= nullptr";
			continue;
		}

		if (GetRuntimeClassName(prop.Value()) == L"Windows.Foundation.IReference`1<Boolean>")
		{
			qDebug() << " " << prop.Key() << "=" << prop.Value().as<IReference<bool>>().Value();
		}
		else if (prop.Key() == L"System.ItemNameDisplay")
		{
			qDebug() << " " << prop.Key() << "=" << prop.Value().as<IReference<hstring>>().Value();
		}
		else
		{
			qDebug() << " " << prop.Key() << "class" << GetRuntimeClassName(prop.Value());
		}
	}
}

std::wstring join(std::vector<hstring> vector, const std::wstring& separator)
{
	if (vector.empty())
	{
		return L"";
	}

	std::wostringstream ss;
	for (size_t index = 0; index < vector.size() - 1; index++)
	{
		ss << std::wstring(vector[index]);
		ss << separator;
	}

	ss << std::wstring(vector.back());
	return ss.str();
}

WiFiDirectUtils::WiFiDirectUtils()
{
}
