#include <cstdlib>
#include <iostream>
#include <random>
#include <winrt/base.h>
#include <winrt/Windows.Devices.h>
#include <winrt/Windows.Devices.WiFiDirect.h>
#include <winrt/Windows.Devices.Enumeration.h>
#include <QDebug>
#include <QtCore/QCoreApplication>
#include <ppltasks.h>
#include <chrono>
#include <memory>
#include "watcher.h"
#include "wifidirectutils.h"

using namespace winrt;
using namespace winrt::Windows::Foundation;
using namespace winrt::Windows::Devices::WiFiDirect;
using namespace winrt::Windows::Devices::Enumeration;

std::mutex mutex;
std::unique_ptr<WiFiDirectDeviceWatcher> watcher;

static bool isPaired()
{
	std::unique_lock<std::mutex> lock(mutex);
	return watcher->discoveredDevice().Pairing().IsPaired();
}

static bool canPair()
{
	std::unique_lock<std::mutex> lock(mutex);
	return watcher->discoveredDevice().Properties().Lookup(L"System.Devices.Aep.CanPair").as<IReference<bool>>().Value();
}

static void waitForPair(bool pair)
{
	if (isPaired() != pair)
	{
		qDebug() << "Waiting for device information update";
		while (isPaired() != pair)
		{
			Sleep(1000);
		}
		qDebug() << "Device information updated!";
	}
}

static void waitForCanPair()
{
	if (canPair())
	{
		return;
	}

	auto start = std::chrono::steady_clock::now();
	std::mutex mutex;
	std::unique_lock<std::mutex> lock(mutex);
	qDebug() << "Waiting 10 minutes for CanPair=true";
	watcher->deviceUpdated().wait_for(lock, std::chrono::minutes(10), canPair);

	if (!canPair())
	{
		qDebug() << "Timeout waiting for CanPair=true, device watcher status" << watcher->status();
		qDebug() << "Restart DeviceWatcher and wait additional 10 minutes for CanPair=true";
		watcher->rediscover();
		watcher->deviceUpdated().wait_for(lock, std::chrono::minutes(10), canPair);
	}

	if (canPair())
	{
		qDebug() << "Received CanPair=true after"
			<< std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() << "ms";
	}
	else
	{
		throw std::runtime_error("Waiting for CanPair=true timeout");
	}
}

static void pair()
{
	waitForCanPair();

	WiFiDirectConnectionParameters connectionParams = WiFiDirectConnectionParameters();
	connectionParams.GroupOwnerIntent(4);

	qDebug() << "PairAsync called";
	std::mutex mutex;
	std::condition_variable completed;
	const DeviceInformationCustomPairing& customPairing = watcher->discoveredDevice().Pairing().Custom();
	auto start = std::chrono::steady_clock::now();
	auto pairAsync = customPairing.PairAsync(DevicePairingKinds::ConfirmOnly, DevicePairingProtectionLevel::Default, connectionParams);
	auto task = Concurrency::create_task([&]()
	{
		try
		{
			DevicePairingResultStatus status = pairAsync.get().Status();
			if (status == DevicePairingResultStatus::Paired)
			{
				qDebug() << "Devices successfully paired";
				waitForPair(true);
			}
			else
			{
				qDebug() << "Pair failed, status" << status;
				waitForPair(false);
			}

			qDebug() << "PairAsync completion notification";
			completed.notify_one();
		}
		catch (...)
		{
			qDebug() << "PairAsync exception happend";
			completed.notify_one();
			throw;
		}
	});

	std::unique_lock<std::mutex> lock(mutex);
	auto completedPredicate = [&]() {return pairAsync.Status() != AsyncStatus::Started; };
	completed.wait_for(lock, std::chrono::seconds(30), completedPredicate);

	if (!completedPredicate())
	{
		qDebug() << "PairAsync 30 seconds timeout, waiting more for 5 minutes";
		qDebug() << "Device watcher status" << watcher->status();
		completed.wait_for(lock, std::chrono::minutes(5), completedPredicate);
	}

	if (!completedPredicate())
	{
		qDebug() << "PairAsync 5 minutes timeout, waiting more for 5 minutes";
		qDebug() << "Device watcher status" << watcher->status();
		completed.wait_for(lock, std::chrono::minutes(5), completedPredicate);
	}

	if (!completedPredicate())
	{
		qDebug() << "PairAsync 5 minutes timeout, Cancel and wait for additional 5 minutes";
		qDebug() << "Device watcher status" << watcher->status();
		pairAsync.Cancel();
		completed.wait_for(lock, std::chrono::minutes(5), completedPredicate);
	}

	if (!completedPredicate())
	{
		qDebug() << "Device watcher status" << watcher->status();
		throw std::runtime_error("PairAsync timeout after all reties");
	}

	qDebug() << "Waiting for pair task to complete:";
	qDebug() << "  PairAsync status" << pairAsync.Status();
	qDebug() << "  Task is done: " << task.is_done();
	qDebug() << "  Device watcher status" << watcher->status();
	task.wait();
	if (pairAsync.Status() == AsyncStatus::Completed)
	{
		qDebug() << "PairAsync task completed";
	}
	else
	{
		qDebug() << "PairAsync task status" << pairAsync.Status();
	}
	qDebug() << "PairAsync finished after"
		<< std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() << "ms";
}

static void unpair()
{
	if (!isPaired())
	{
		return;
	}

	qDebug() << "UnpairAsync called";
	DeviceUnpairingResultStatus status = watcher->discoveredDevice().Pairing().UnpairAsync().get().Status();
	if (status == DeviceUnpairingResultStatus::Unpaired)
	{
		qDebug() << "Device successfully unpaired";
	}
	else
	{
		qDebug() << "Unpair failed, status" << status;
	}

	waitForPair(false);
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	installAppMessageHandler();
	init_apartment();

	Settings settings(argv[0]);

	try
	{
		std::vector<hstring> deviceNames = settings.devicesToFind();
		watcher = std::make_unique<WiFiDirectDeviceWatcher>(deviceNames, mutex);
		watcher->discover();
		if (watcher->discoveredDevice() == nullptr)
		{
			qDebug() << "Failed to find devices:" << join(deviceNames);
			throw std::runtime_error("");
		}

		const DeviceInformationCustomPairing& customPairing = watcher->discoveredDevice().Pairing().Custom();
		auto pairingRequested = customPairing.PairingRequested([&](const DeviceInformationCustomPairing& sender, const DevicePairingRequestedEventArgs& args)
		{
			std::unique_lock<std::mutex> lock(mutex);
			qDebug() << "Pairing requested...";
			if (args.PairingKind() == DevicePairingKinds::ConfirmOnly)
			{
				args.Accept();
			}
			else
			{
				qDebug() << "Invalid pairing kind" << static_cast<int>(args.PairingKind());
				throw std::runtime_error("");
			}
		});

		std::default_random_engine randomGenerator;
		std::uniform_int_distribution<int> unpairPairTimeout(500, 2000);
		std::uniform_int_distribution<int> pairUnpairTimeout(500, 3000);

		while (true)
		{
			int timeout;

			unpair();

			timeout = unpairPairTimeout(randomGenerator);
			qDebug() << "Sleeping" << timeout << "ms";
			Sleep(timeout);

			pair();

			if (isPaired())
			{
				timeout = pairUnpairTimeout(randomGenerator);
				qDebug() << "Sleeping" << timeout << "ms";
				Sleep(timeout);
			}
		}
	}
	catch (const std::exception& e)
	{
		qDebug() << e.what();
		throw;
	}
	catch (...)
	{
		qDebug() << "Unknown exception";
		throw;
	}

	return EXIT_SUCCESS;
}
