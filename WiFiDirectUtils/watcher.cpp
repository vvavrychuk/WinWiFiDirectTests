#include <winrt/Windows.Devices.WiFiDirect.h>
#include <QDebug>
#include "watcher.h"
#include "wifidirectutils.h"

using namespace winrt::Windows::Devices::WiFiDirect;

WiFiDirectDeviceWatcher::WiFiDirectDeviceWatcher(const std::vector<hstring>& deviceNames, std::mutex& mutex)
	: _deviceNames(deviceNames), _mutex(mutex)
{
	_deviceWatcher = DeviceInformation::CreateWatcher(
		WiFiDirectDevice::GetDeviceSelector(WiFiDirectDeviceSelectorType::AssociationEndpoint));
	_deviceWatcher.Added({ this, &WiFiDirectDeviceWatcher::onDeviceAdded });
	_deviceWatcher.Updated({ this, &WiFiDirectDeviceWatcher::onDeviceUpdated });
	_deviceWatcher.Removed({ this, &WiFiDirectDeviceWatcher::onDeviceRemoved });
	_deviceWatcher.EnumerationCompleted({ this, &WiFiDirectDeviceWatcher::onEnumerationCompleted });
	_deviceWatcher.Stopped({ this, &WiFiDirectDeviceWatcher::onStopped });
}

void WiFiDirectDeviceWatcher::discover()
{
	qDebug() << "Finding devices:" << join(_deviceNames);
	if ((_deviceWatcher.Status() == DeviceWatcherStatus::Created) ||
		(_deviceWatcher.Status() == DeviceWatcherStatus::Stopped) ||
		(_deviceWatcher.Status() == DeviceWatcherStatus::Aborted))
	{
		_deviceWatcher.Start();
	}

	std::unique_lock<std::mutex> lock(_completedMutex);
	while ((_deviceWatcher.Status() != DeviceWatcherStatus::Aborted) &&
		(_discoveredDevice == nullptr))
	{
		if (_deviceWatcher.Status() == DeviceWatcherStatus::EnumerationCompleted)
		{
			_deviceWatcher.Stop();
		}
		if (_deviceWatcher.Status() == DeviceWatcherStatus::Stopped)
		{
			_deviceWatcher.Start();
		}
		_completed.wait(lock);
	}
}

void WiFiDirectDeviceWatcher::rediscover()
{
	_discoveredDevice = nullptr;
	discover();
}

void WiFiDirectDeviceWatcher::onDeviceAdded(const DeviceWatcher& deviceWatcher, const DeviceInformation& deviceInfo)
{
	qDebug() << "Found" << deviceInfo.Id() << deviceInfo.Name();
	for (hstring deviceName : _deviceNames)
	{
		if (deviceInfo.Name() == deviceName)
		{
			dumpProperties(deviceInfo.Properties());
			_discoveredDevice = deviceInfo;
			_completed.notify_one();
			_deviceUpdatedEvent.notify_all();
		}
	}
}

void WiFiDirectDeviceWatcher::onDeviceUpdated(const DeviceWatcher& deviceWatcher, const DeviceInformationUpdate& deviceInfo)
{
	std::unique_lock<std::mutex> lock(_mutex);
	qDebug() << "Updated" << deviceInfo.Id();
	if ((_discoveredDevice != nullptr) && (deviceInfo.Id() == _discoveredDevice.Id()))
	{
		dumpProperties(deviceInfo.Properties());
		_discoveredDevice.Update(deviceInfo);
		_deviceUpdatedEvent.notify_all();
	}
}

void WiFiDirectDeviceWatcher::onDeviceRemoved(const DeviceWatcher& deviceWatcher, const DeviceInformationUpdate& deviceInfo)
{
	std::unique_lock<std::mutex> lock(_mutex);
	qDebug() << "Removed" << deviceInfo.Id();
}

void WiFiDirectDeviceWatcher::onEnumerationCompleted(const DeviceWatcher& deviceWatcher, const Windows::Foundation::IInspectable& object)
{
	qDebug() << "Enumeration completed";
	_completed.notify_one();
}

void WiFiDirectDeviceWatcher::onStopped(const DeviceWatcher& deviceWatcher, const Windows::Foundation::IInspectable& object)
{
	qDebug() << "Enumeration stopped";
	_completed.notify_one();
}
