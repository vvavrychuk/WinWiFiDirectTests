#include <cstdlib>
#include <iostream>
#include <fstream>
#include <random>
#include <winrt/base.h>
#include <winrt/Windows.Devices.h>
#include <winrt/Windows.Devices.WiFiDirect.h>
#include <winrt/Windows.Devices.Enumeration.h>
#include <QDebug>
#include <QMutex>
#include <QtCore/QCoreApplication>

using namespace winrt;
using namespace winrt::Windows::Foundation;
using namespace winrt::Windows::Foundation::Collections;
using namespace winrt::Windows::Devices::WiFiDirect;

QtMessageHandler defaultMessageHandler;

QDebug operator<<(QDebug dbg, const hstring& str)
{
	return dbg << QString::fromWCharArray(str.c_str());
}

QDebug operator<<(QDebug dbg, WiFiDirectAdvertisementPublisherStatus status)
{
	switch (status)
	{
	case WiFiDirectAdvertisementPublisherStatus::Created: return dbg << "Created";
	case WiFiDirectAdvertisementPublisherStatus::Started: return dbg << "Started";
	case WiFiDirectAdvertisementPublisherStatus::Stopped: return dbg << "Stopped";
	case WiFiDirectAdvertisementPublisherStatus::Aborted: return dbg << "Aborted";
	}
}

QDebug operator<<(QDebug dbg, WiFiDirectError error)
{
	switch (error)
	{
	case WiFiDirectError::Success: return dbg << "Success";
	case WiFiDirectError::RadioNotAvailable: return dbg << "RadioNotAvailable";
	case WiFiDirectError::ResourceInUse: return dbg << "ResourceInUse";
	}
}

void appMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	static QMutex mutex;
	QMutexLocker lock(&mutex);

	static std::ofstream logFile("logFile.txt");
	if (logFile)
	{
		logFile << qPrintable(qFormatLogMessage(type, context, msg)) << std::endl;
	}

	if (defaultMessageHandler != NULL)
	{
		defaultMessageHandler(type, context, msg);
	}
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	init_apartment();
	auto publisher = WiFiDirectAdvertisementPublisher();
	publisher.Advertisement().IsAutonomousGroupOwnerEnabled(false);
	publisher.Advertisement().ListenStateDiscoverability(WiFiDirectAdvertisementListenStateDiscoverability::Normal);
	publisher.StatusChanged([](const WiFiDirectAdvertisementPublisher& publisher, const WiFiDirectAdvertisementPublisherStatusChangedEventArgs& args)
	{
		qDebug() << "Status" << args.Status() << "Error" << args.Error();
	});

	auto listener = WiFiDirectConnectionListener();
	listener.ConnectionRequested([](const WiFiDirectConnectionListener& listener, const WiFiDirectConnectionRequestedEventArgs& args)
	{
		qDebug() << "Connection request from" << args.GetConnectionRequest().DeviceInformation().Name();
	});

	publisher.Start();
	qDebug() << "Publisher started";

	return a.exec();
}
 