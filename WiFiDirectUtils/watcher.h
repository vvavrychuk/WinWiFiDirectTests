#pragma once
#include <vector>
#include <winrt/base.h>
#include <winrt/Windows.Devices.Enumeration.h>

using namespace winrt;
using namespace winrt::Windows::Devices::Enumeration;

class WiFiDirectDeviceWatcher
{
public:
	WiFiDirectDeviceWatcher(const std::vector<hstring>& deviceNames, std::mutex& mutex);

	void discover();
	void rediscover();

	DeviceWatcherStatus status()
	{
		return _deviceWatcher.Status();
	}

	DeviceInformation& discoveredDevice()
	{
		return _discoveredDevice;
	}

	std::condition_variable& deviceUpdated()
	{
		return _deviceUpdatedEvent;
	}

private:
	void onDeviceAdded(const DeviceWatcher& deviceWatcher, const DeviceInformation& deviceInfo);
	void onDeviceUpdated(const DeviceWatcher& deviceWatcher, const DeviceInformationUpdate& deviceInfo);
	void onDeviceRemoved(const DeviceWatcher& deviceWatcher, const DeviceInformationUpdate& deviceInfo);
	void onEnumerationCompleted(const DeviceWatcher& deviceWatcher, const Windows::Foundation::IInspectable& object);
	void onStopped(const DeviceWatcher& deviceWatcher, const Windows::Foundation::IInspectable& object);

	std::mutex& _mutex;
	std::vector<hstring> _deviceNames;
	DeviceWatcher _deviceWatcher = nullptr;
	DeviceInformation _discoveredDevice = nullptr;
	std::condition_variable _deviceUpdatedEvent;

	std::condition_variable _completed;
	std::mutex _completedMutex;
};