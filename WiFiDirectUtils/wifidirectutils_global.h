#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(WIFIDIRECTUTILS_LIB)
#  define WIFIDIRECTUTILS_EXPORT Q_DECL_EXPORT
# else
#  define WIFIDIRECTUTILS_EXPORT Q_DECL_IMPORT
# endif
#else
# define WIFIDIRECTUTILS_EXPORT
#endif
